#!/usr/bin/env bash

set -e

HOME_FOLDER='/opt/control-center'
DOCKER_COMPOSE_FILE='control-center.yml'
STACK_NAME='quizme'
DOCKER_REGISTRY_HOST='registry.gitlab.com'
DOCKER_REGISTRY=${DOCKER_REGISTRY_HOST}
DOCKER_REGISTRY_LOGIN='dev-ops'
DOCKER_REGISTRY_API_KEY='xuU-Z86y5N82rYsv8WGa'


help() {
  echo
  echo '\
Usage: ./deploy.sh start                      Starts a stack using given IP as advertised
       ./deploy.sh status                     Is any of lion stack services running.
       ./deploy.sh stop                       Stops all services in a stack and removes containers
       ./deploy.sh update_service service     Forces update of a given service.'
    echo
}

start () {
    local res=0
    docker stack ps ${STACK_NAME} > /dev/null 2>&1 || res=1
    if [[ ${res} -eq 0 ]]; then
        echo "Stack is already running..."
        exit 1
    fi
    docker_login
    pull_images
    deploy
}


docker_login() {
    #Login information for the git lab
    docker login -u ${DOCKER_REGISTRY_LOGIN} -p ${DOCKER_REGISTRY_API_KEY} ${DOCKER_REGISTRY_HOST}
}

pull_images() {
      #Downloading Images from Repository
    imageNames=$(grep -F "image: " *.yml | grep -Fv "#" | awk '{print $2}')
    echo "All images being downloaded are: \n${imageNames}"
    for imageName in $imageNames; do
        echo "Pulling image from artifactory: $imageName"
    local image="$(echo -e "${imageName}" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')"
    echo $(docker image pull $image)
    done
    sleep 5
}

status() {
    local res=0
    docker stack ps ${STACK_NAME} || res=1
    if [[ ${res} -eq 0 ]]; then
        echo "Stack is running..."
        exit 1
    fi
}

stop () {
    # logs
    docker stack rm  ${STACK_NAME}
}
deploy() {
    echo 'Deploying stack...'
    docker stack deploy --compose-file ${DOCKER_COMPOSE_FILE} --with-registry-auth ${STACK_NAME}
    echo 'Deployed stack successfully'
    sleep 3
}

update-internal() {
    SERVICE_WITH_IMAGE=$1
    FORCE_UPDATE=$2
    SERVICE=$(echo ${SERVICE_WITH_IMAGE} | cut -f1 -d';')
    IMAGE=$(echo ${SERVICE_WITH_IMAGE} | cut -f2 -d';')
    IMAGE_ID_OLD=$(docker image inspect ${IMAGE} --format='{{.ID}}')
    docker image pull ${IMAGE}
    IMAGE_ID_NEW=$(docker image inspect ${IMAGE} --format='{{.ID}}')
    if [[ ${IMAGE_ID_OLD} != ${IMAGE_ID_NEW} ]] || [[ '${FORCE_UPDATE}' == 'force' ]]; then
      docker service update --force --image ${IMAGE} ${SERVICE}
    fi
}

update_service() {
    docker_login
    SERVICE_NAME=$1
    SERVICE_WITH_IMAGE=$(docker stack services ${STACK_NAME} --format '{{.Name}};{{.Image}}' --filter='name=${STACK_NAME}_${SERVICE_NAME}')
    update-internal ${SERVICE_WITH_IMAGE} 'force'
}

clean-up() {
    docker system prune -a -f
}

case $1 in
    start)
        start
    ;;
    status)
        status
    ;;
    update_service)
        update_service $2
    ;;
    stop)
        stop
    ;;
    *)
        help
    ;;
esac


exit 0
