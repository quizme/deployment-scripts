# deployment-setup
This package will help us to deploy restaurant-service backend and frontend environment images from gitlab container registry

# Gitlab access credentials
You can find the access credentials in deploy.sh as variables
```
DOCKER_REGISTRY_LOGIN
DOCKER_REGISTRY_API_KEY
```

# prerequisite 
**docker** should up and run on the host system 

# steps to start 
Download and execute **deploy.sh** 
```
./deploy.sh start
```
Usage:

       ./deploy.sh start                      Starts a stack

       ./deploy.sh status                     Is any of stack services running.

       ./deploy.sh stop                       it stops and removes the stack

       ./deploy.sh update_service service     Forces update of a given service


# Check stack is up and running
```
docker stack ls
```

# List the docker services 
```
docker service ls
```


**Note:**

You might need to add host entries to access restaurant-service and ui-service

if you get the error message "This node is not a swarm manager. Use **"docker swarm init"** or **"docker swarm join"** to connect this node to swarm and try again",
please execute the command "**docker swarm init**".

